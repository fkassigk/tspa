
# LIGHTest

Lightweight Infrastructure for Global Heterogeneous Trust management in support of an open Ecosystem of Stakeholders and Trust schemes.

For more information please visit the LIGHTest website: https://www.lightest.eu/


# Documentation

See the [LIGHTest Deliverables](https://www.lightest.eu/downloads/pub_deliverables/index.html).


# Requirements

* Java 1.17 or newer
* Maven ~3.6.0
* Internet access 
* TRAIN Trust Infrastructure
  * Zone Manager



# How to use it




## Example Usage

```java
```


## Maven

You can use the ATV library as a Maven dependency

```xml
<dependency>
    <groupId>eu.xfsc.train</groupId>
    <artifactId>tspa</artifactId>
    <version>1.x.x-SNAPSHOT</version>
</dependency>
```


# Licence
* Apache License 2.0 (see [LICENSE](./LICENSE))

